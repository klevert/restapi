from django.contrib import admin

from backend.models import Customer, Order

admin.site.register(Customer)
admin.site.register(Order)
