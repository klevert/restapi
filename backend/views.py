from django.contrib.auth.models import User
from rest_framework import permissions, viewsets
from backend.models import Customer, Order
from backend.permissions import IsOwnerOrReadOnly
from backend.serializers import CustomerSerializer, UserSerializer, OrderSerializer

class CustomerViewSet(viewsets.ModelViewSet):
    """
        This viewset automatically provides `list`, `create`, `update` and `delete` actions.

    """
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class OrderViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `update` and `delete` actions.
    """
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
