from django.contrib.auth.models import User
from rest_framework import serializers

from backend.models import Customer, Order


class CustomerSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    orders = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Customer
        fields = ['url', 'id', 'first_name', 'last_name', 'email', 'owner', 'orders', 'created_at']


class UserSerializer(serializers.HyperlinkedModelSerializer):
    customers = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    orders = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = User
        fields = ['url', 'id', 'username', 'orders', 'customers']


class OrderSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = Order
        fields = ['url', 'id', 'description', 'owner', 'customer', 'created_at']
