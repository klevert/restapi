from django.db import models


class Customer(models.Model):
    class Meta:
        db_table = 'customers'

    first_name = models.CharField("First name", max_length=255)
    last_name = models.CharField("Last name", max_length=255)
    email = models.EmailField()
    owner = models.ForeignKey('auth.User', related_name='customers', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.id}: {self.last_name} {self.first_name}"


class Order(models.Model):
    class Meta:
        db_table = 'orders'

    description = models.TextField(max_length=255)
    owner = models.ForeignKey('auth.User', related_name='orders', on_delete=models.CASCADE)
    customer = models.ForeignKey('Customer', related_name='orders', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"order {self.id}"
